﻿using UnityEngine;
using System.Collections;

public class CameraController : MonoBehaviour
{
    public Component Target;

    // Use this for initialization
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        if (Target != null)
        {
            if (Input.GetMouseButton(0))
            {
                const float horizontalSpeed = 4.0f;
                const float verticalSpeed = 4.0f;

                var h = horizontalSpeed * Input.GetAxis("Mouse X");
                var v = verticalSpeed * Input.GetAxis("Mouse Y");

                transform.RotateAround(Target.transform.position, Vector3.up, h);
                transform.RotateAround(Target.transform.position, transform.right, -v);
            }

            const float zoomSpeed = 10.0f;
            var zoom = zoomSpeed * Input.GetAxis("Mouse ScrollWheel");
            transform.Translate(0, 0, zoom, transform);
        }
    }
}
